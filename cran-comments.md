## Resubmission
This is a resubmission. In this version, I have:

* Fixed GCC compilation errors for gcc5 -std=gnu++14 and gcc6

## Test environments
* OS X Yosemite, R 3.2.3
* ubuntu 12.04, R 3.2.3

## R CMD check results
There were no ERRORs or WARNINGs. 

## Downstream dependencies
There are currently no downstream dependencies for this package.
